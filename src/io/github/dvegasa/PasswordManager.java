package io.github.dvegasa;

import java.security.MessageDigest;

/**
 * 16.06.2018
 */

class PasswordManager {

    private static final int MIN = 10;
    private static final int MAX = Integer.MAX_VALUE;

    public static String prepare(String password){
        int salt = MIN + (int) (Math.random() * MAX);
        password += salt;
        String hash = sha256(password);
        return salt+";"+hash;
    }

    public static String sha256(String base) {
        try{
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.getBytes("UTF-8"));
            StringBuilder hexString = new StringBuilder();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if(hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }
}
