package io.github.dvegasa;

import com.sun.istack.internal.Nullable;

/**
 * 15.06.2018
 */

class Manager {

    public static int ANSWER_REG_GOOD = 200;
    public static int ANSWER_DEL_GOOD = 201;
    public static int ANSWER_HELP_GOOD = 202;
    public static int ANSWER_GET_GOOD = 203;
    public static int ANSWER_GET_MAP_IS_EMPTY = 204;
    public static int ANSWER_LIN_GOOD = 205;

    public static int ERROR_MISSING_COLON = 253;
    public static int ERROR_REG_LOGIN_ALREADY_EXISTS = 254;
    public static int ERROR_GET_NOT_REGISTRED = 255;
    public static int ERROR_LIN_USERNAME_NOT_REGISTRED = 256;
    public static int ERROR_LIN_PASSWORD_DOES_NOT_MATCH = 257;
    public static int ERROR_REG_USERNAME_ZERO_LENGTH = 258;
    public static int ERROR_REG_PASSWORD_ZERO_LENGTH = 259;
    public static int ERROR_DEL_NOT_REGISTRED = 260;

    private StorageWorker storageWorker;
    private Handler handler;

    interface StorageWorker {
        boolean init();

        void registration(String username, String password);

        boolean login(String username, String password);

        String get(String username);

        String get();

        void delete(String username);

        boolean isContain(String username);

        boolean isEmpty();
    }

    interface Handler {
        void handleAnswer(int code, @Nullable String message);

        void handleError(int errorCode);
    }

    public Manager() {
    }

    public void setStorageWorker(StorageWorker storageWorker) {
        this.storageWorker = storageWorker;
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    public boolean initStorage(){
        return storageWorker.init();
    }

    public void reg(String input) {
        String username = getUsername(input);
        String password = getPassword(input);
        if (username == null || password == null) {
            handler.handleError(ERROR_MISSING_COLON);
            return;
        }

        if (username.length() == 0) {
            handler.handleError(ERROR_REG_USERNAME_ZERO_LENGTH);
            return;
        }

        if (password.length() == 0) {
            handler.handleError(ERROR_REG_PASSWORD_ZERO_LENGTH);
            return;
        }

        if (storageWorker.isContain(username)) {
            handler.handleError(ERROR_REG_LOGIN_ALREADY_EXISTS);
            return;
        }

        storageWorker.registration(username, PasswordManager.prepare(password));
        handler.handleAnswer(ANSWER_REG_GOOD, username);

    }

    public void del(String input) {
        input += ':';
        if (storageWorker.isContain(getUsername(input))) {
            storageWorker.delete(getUsername(input));
            handler.handleAnswer(ANSWER_DEL_GOOD, null);

        } else {
            handler.handleError(ERROR_DEL_NOT_REGISTRED);
        }
    }

    public void lin(String input) {
        if (storageWorker.isContain(getUsername(input))) {
            if (storageWorker.login(getUsername(input), getPassword(input))) {
                handler.handleAnswer(ANSWER_LIN_GOOD, null);
            } else {
                handler.handleError(ERROR_LIN_PASSWORD_DOES_NOT_MATCH);
            }
        } else {
            handler.handleError(ERROR_LIN_USERNAME_NOT_REGISTRED);
        }
    }

    public void get() {
        if (storageWorker.isEmpty()) {
            handler.handleAnswer(ANSWER_GET_MAP_IS_EMPTY, null);
        } else {
            handler.handleAnswer(ANSWER_GET_GOOD, storageWorker.get());
        }
    }

    public void get(String input) {
        if (input.length() == 0) {
            get();
            return;
        }

        input += ':';
        if (storageWorker.isContain(getUsername(input))) {
            handler.handleAnswer(ANSWER_GET_GOOD, storageWorker.get(getUsername(input)));
        } else {
            handler.handleError(ERROR_GET_NOT_REGISTRED);
        }
    }

    public void help() {
        String help =
                "> 'reg/LOGIN:PASS' -- register new user\n" +
                        "> 'lin/LOGIN:PASS' -- check using name and password\n" +
                        "> 'get/'           -- get all users and their hashes\n" +
                        "> 'get/LOGIN'      -- get login and hash for current user\n" +
                        "> 'del/LOGIN'      -- delete user with LOGIN\n" +
                        "> 'stop/           -- stop the program";
        handler.handleAnswer(ANSWER_HELP_GOOD, help);
    }

    private String getUsername(String input) {
        int index = input.indexOf(':');
        if (index < 0) {
            return null;
        }
        String username = input.substring(0, index);
        return username;
    }

    private String getPassword(String input) {
        int index = input.indexOf(':');
        if (index < 0) {
            return null;
        }
        String password = input.substring(index + 1, input.length());
        return password;
    }

    public void makeError(int code) {
        handler.handleError(code);
    }
}
