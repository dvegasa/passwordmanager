package io.github.dvegasa;

/**
 * 16.06.2018
 */

class Handler implements Manager.Handler {
    private static final String ERROR_PREFIX = "> [ERROR] ";

    public Handler(Manager manager) {
        manager.setHandler(this);
    }

    @Override
    public void handleError(int errorCode) {
        String outputText = "";
        outputText += ERROR_PREFIX;

        if (errorCode == Main.ERROR_MISSING_SLASH) {
            outputText += "You forgot '/'";

        } else if (errorCode == Main.ERROR_INIT_STORAGE) {
            outputText +="The storage could not be initialized.\n          The program is stopped";

        } else if (errorCode == Main.ERROR_UNKNOWN_COMMAND) {
            outputText += "Wrong command. Type 'help/' for get list of all commands";

        } else if (errorCode == Manager.ERROR_MISSING_COLON) {
            outputText += "Missing ':'. Separate username and password with ':'";

        } else if (errorCode == Manager.ERROR_REG_LOGIN_ALREADY_EXISTS) {
            outputText += "Login already exists";

        } else if (errorCode == Manager.ERROR_GET_NOT_REGISTRED) {
            outputText += "This user is not registered";

        } else if (errorCode == Manager.ERROR_LIN_PASSWORD_DOES_NOT_MATCH) {
            outputText += "Password doesn't match";

        } else if (errorCode == Manager.ERROR_LIN_USERNAME_NOT_REGISTRED) {
            outputText += "This user is not registered";

        } else if (errorCode == Manager.ERROR_REG_PASSWORD_ZERO_LENGTH) {
            outputText += "Parameters must be non zero length";

        } else if (errorCode == Manager.ERROR_REG_USERNAME_ZERO_LENGTH) {
            outputText += "Parameters must be non zero length";

        } else if (errorCode == Manager.ERROR_DEL_NOT_REGISTRED) {
            outputText += "This user is not registered";

        } else {
            outputText += "Unknown error. Code: " + errorCode;
        }

        System.out.println(outputText);
    }

    @Override
    public void handleAnswer(int code, String message) {
        if (code == Manager.ANSWER_HELP_GOOD) {
            System.out.println(message);

        } else if (code == Manager.ANSWER_REG_GOOD) {
            System.out.println("> '" + message + "' was successfully registered");

        } else if (code == Manager.ANSWER_GET_GOOD) {
            System.out.println(message);

        } else if (code == Manager.ANSWER_GET_MAP_IS_EMPTY) {
            System.out.println("> There is no registered users");

        } else if (code == Manager.ANSWER_LIN_GOOD) {
            System.out.println("> Good");

        } else if (code == Manager.ANSWER_DEL_GOOD) {
            System.out.println("> Deleted");

        } else {
            System.out.println("Unknown answer. Code: " + code);
        }
    }
}
