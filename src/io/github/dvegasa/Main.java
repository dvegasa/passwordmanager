package io.github.dvegasa;

import java.util.Scanner;

public class Main {
    private static Manager manager;
    private static Scanner scanner;
    private static Handler handler;

    public static final int ERROR_MISSING_SLASH = 100;
    public static final int ERROR_UNKNOWN_COMMAND = 101;
    public static final int ERROR_INIT_STORAGE = 102;

    private static final String READY_HINT = "> === READY === ";
    private static final String LAUNCHING_HINT = "> Launching...";

    public static void main(String[] args) {
        System.out.println(LAUNCHING_HINT);
        init();

        if (manager.initStorage()) {
            System.out.println(READY_HINT);
        }else {
            handler.handleError(ERROR_INIT_STORAGE);
            return;
        }

        while (true) {
            String input = scanner.nextLine();
            if (input.equals("stop/")) break;
            defineCommand(input);
        }
    }

    private static void init() {
        scanner = new Scanner(System.in);
//        manager = new StorageMap().getManager();
        manager = new Manager();
        StorageMap storageMap = new StorageMap();
        storageMap.addStorageWorker(manager);
        handler = new Handler(manager);
    }

    private static void defineCommand(String input) {

        int index = input.indexOf('/');

        if (index < 0) {
            manager.makeError(ERROR_MISSING_SLASH);
            return;
        }

        String command = input.substring(0, index);
        String params = input.substring(index + 1, input.length());


        switch (command) {
            case "reg": {
                manager.reg(params);
                break;
            }

            case "del": {
                manager.del(params);
                break;
            }

            case "lin": {
                manager.lin(params);
                break;
            }

            case "get": {
                manager.get(params);
                break;
            }

            case "help": {
                manager.help();
                break;
            }

            default: {
                manager.makeError(ERROR_UNKNOWN_COMMAND);
                break;
            }
        }
    }
}
