package io.github.dvegasa;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.Scanner;

/**
 * 16.06.2018
 */

class FileManager {
    private static final String FILE_NAME = "users.txt";

    public static HashMap<String, String> getMap() throws Exception {

        HashMap<String, String> map = new HashMap<>();

        File f = new File(FILE_NAME);

        if (!(f.exists() && !f.isDirectory())) {
            writeMapToFile(new HashMap<>());
        }

        FileReader fr = new FileReader(FILE_NAME);
        Scanner scanner = new Scanner(fr);

        while (scanner.hasNextLine()){
            String line = scanner.nextLine();
            int index1 = line.indexOf('-');

            String username = line.substring(2, index1);
            String salthash = line.substring(index1+1, line.length());
            map.put(username, salthash);
        }

        return map;
    }

    public static void writeMapToFile(HashMap<String, String> map) throws Exception {
        String string;
        FileWriter fw = new FileWriter(FILE_NAME);
        string = StorageMap.mapToString(map);
        fw.write(string);
        fw.close();
    }

    public static void addUserToFile(String userInfo) throws Exception {
        FileWriter fw = new FileWriter(FILE_NAME, true);
        fw.append(userInfo).append("\n");
        fw.close();
    }
}
