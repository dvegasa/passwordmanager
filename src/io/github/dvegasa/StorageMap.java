package io.github.dvegasa;

import java.util.HashMap;
import java.util.Map;

/**
 * 15.06.2018
 */

class StorageMap implements Manager.StorageWorker {

    private HashMap<String, String> map = new HashMap<>();

    public Manager getManager() {
        Manager manager = new Manager();
        manager.setStorageWorker(this);
        return manager;
    }

    public void addStorageWorker(Manager manager){
        manager.setStorageWorker(this);
    }

    @Override
    public boolean init() {
        try {
            map = FileManager.getMap();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public void registration(String username, String hashedPassword) {
        map.put(username, hashedPassword);
        try {
            FileManager.addUserToFile(get(username));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean login(String username, String password) {
        String salthash = map.get(username);

        int index = salthash.indexOf(';');
        int salt = Integer.parseInt(salthash.substring(0, index));
        String hash = salthash.substring(index + 1, salthash.length());

        password += salt;
        String hashedPassword = PasswordManager.sha256(password);
        return hash.equals(hashedPassword);
    }

    @Override
    public String get(String username) {
        String password = map.get(username);

        return "> " + username + "-" + password;
    }

    @Override
    public String get() {

        return mapToString(map);
    }

    public static String mapToString(HashMap<String, String> map1) {
        StringBuilder answer = new StringBuilder();

        for (Map.Entry<String, String> entry : map1.entrySet()) {
            String username = entry.getKey();
            String password = entry.getValue();
            answer.append("> ").append(username).append("-").append(password).append("\n");
        }
        return answer.toString();
    }

    @Override
    public void delete(String username) {
        map.remove(username);
        try {
            FileManager.writeMapToFile(map);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isContain(String username) {
        return map.containsKey(username);
    }

    @Override
    public boolean isEmpty() {
        return map.isEmpty();
    }
}
